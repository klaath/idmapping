package org.bilvea.forecast.idmapping;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.ObjectOutputStream;
import java.util.HashSet;

import org.grouplens.lenskit.indexes.IdIndexMapping;

/**
 * @params args[1] original graph file
 * @author kaan bingol
 */
public class SparseToDenseMappping 
{
    public static void main( String[] args )
    {
    	long startTime = System.currentTimeMillis();
    	String oFile = args[0];
		File originalFile = new File(oFile);
		File outFile = new File(oFile + ".out");
		HashSet<Long> IDmap = new HashSet<Long>();
		
		try{
			BufferedReader br =  new BufferedReader(new FileReader(originalFile));
			String line;
			
			while((line = br.readLine()) != null){
				String[] ids = line.split("\t");
				IDmap.add(Long.parseLong(ids[0]));
				IDmap.add(Long.parseLong(ids[1]));
			}
			br.close();
			
			IdIndexMapping mapping = IdIndexMapping.create(IDmap);
			System.out.println("Map Size: " + mapping.size());
			// Serialize data object to a file
			ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(oFile + ".map.ser"));
			out.writeObject(mapping);
			out.close();
			
			br =  new BufferedReader(new FileReader(originalFile));
			BufferedWriter bw = new BufferedWriter(new FileWriter(outFile));
			
			while((line = br.readLine()) != null){
				String[] ids = line.split("\t");
				bw.write(mapping.getIndex(Long.parseLong(ids[0])) + "\t" + mapping.getIndex(Long.parseLong(ids[1])));
				bw.newLine();
			}
			bw.close();
			
		}catch(Exception e){
			e.printStackTrace();
		}

    	long endTime   = System.currentTimeMillis();
		long totalTime = endTime - startTime;
		System.out.println("Running Time: " +totalTime);
    }
}
