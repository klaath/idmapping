package org.bilvea.forecast.idmapping;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.ObjectInputStream;

import org.grouplens.lenskit.indexes.IdIndexMapping;

/**
 * @params args[0] serilized map file
 * 			args[1] PG scores result file
 * @author kaan bingol
 */
public class DenseToSparseMapping {

	public static void main(String[] args) {
		long startTime = System.currentTimeMillis();
		String serFile = args[0];
		String rFile = args[1];
		File resultFile = new File(rFile);
		File tmpout = new File(rFile+".tmp");
		
		try{ 
			FileInputStream door = new FileInputStream(serFile); 
			ObjectInputStream reader = new ObjectInputStream(door); 
			IdIndexMapping mapping = (IdIndexMapping) reader.readObject();
			int mapSize = mapping.size();
			reader.close();
			
			BufferedReader br = new BufferedReader(new FileReader(resultFile));
			BufferedWriter bw = new BufferedWriter(new FileWriter(tmpout));
			String line;
			
			while((line = br.readLine()) != null){
				String[] values = line.split("\t");
				int index = Integer.parseInt(values[0]);
				if(mapSize > index){
					bw.write(mapping.getId(index) + "\t" + values[1]);
					bw.newLine();
				}
			}
			br.close();
			bw.close();
			resultFile.delete();
			tmpout.renameTo(new File(rFile));
			
		} catch(Exception e){
			e.printStackTrace();
		}
		
		long endTime   = System.currentTimeMillis();
		long totalTime = endTime - startTime;
		System.out.println("Running Time: " +totalTime);
	}

}
